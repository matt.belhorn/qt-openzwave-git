# Maintainer: Matt Belhorn <matt.belhorn@gmail.com>

_pkgname_base="qt-openzwave"
_gitcommit=""
_pkgsrc="${_pkgname_base}"

pkgname=${_pkgname_base}-git
pkgver=r156.65ab764
pkgrel=1
pkgdesc="QT5 Wrapper for OpenZWave"
arch=('i686' 'x86_64' 'arm' 'armv6h' 'armv7h' 'aarch64')
url="https://github.com/OpenZWave/qt-openzwave"
license=('LGPL3')
makedepends=('git' 'google-breakpad-git' 'rapidjson')
source=("git+https://github.com/OpenZWave/qt-openzwave.git"
        "breakpad.patch")
depends=('openzwave-git' 'qt5-mqtt' 'qt5-websockets' 'qt5-remoteobjects' 'libunwind')
conflicts=('qt-openzwave')
provides=(
    'ozwdaemon'
    'libqt-openzwave.so'
    'libqt-openzwavedatabase.so'
    "${_pkgname_base}"
    )
sha256sums=(
    'SKIP'
    '65b92e5467853f981b6b93570f4405fac3e6369db73189f469ef4e7a9ec87610'
    )

pkgver() {
  cd "$srcdir/${_pkgsrc}"
  printf "r%s.%s" \
      "$(git rev-list --count HEAD)" \
      "$(git rev-parse --short HEAD)"
}

prepare() {
  cd "$srcdir/${_pkgsrc}"
  if [ -f "Makefile" ]; then
    qmake-qt5 -r
    make distclean
  fi
  patch --forward --strip=1 --input="${srcdir}/breakpad.patch"
  sed -i 's|/usr/local|/usr|g' qt-ozwdaemon/qt-ozwdaemon.pro
}

build() {
  cd "$srcdir/${_pkgsrc}"
  qmake-qt5 -r PREFIX="/usr" \
               QMAKE_CFLAGS="${CFLAGS}" \
               QMAKE_CXXFLAGS="${CXXFLAGS}" \
               QMAKE_LFLAGS="${LDFLAGS}" \
               "CONFIG+=BreakPad"
  make
}

package() {
  cd "$srcdir/${_pkgsrc}"
  make INSTALL_ROOT="${pkgdir}" install
}
